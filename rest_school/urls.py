from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from learn.views import (
    CourseViewSet,
    BlockViewSet,
    TopicViewSet,
    LessonViewSet
)
from rest_framework.schemas import get_schema_view

schema_view = get_schema_view(title='Pastebin API')

router = DefaultRouter()

router.register(r'course', CourseViewSet)
router.register(r'block', BlockViewSet)
router.register(r'topic', TopicViewSet)
router.register(r'lesson', LessonViewSet)

urlpatterns = [
    path(r'api/schema/', schema_view),
    url(r'^api/', include(router.urls)),
    path('admin/', admin.site.urls),
]

urlpatterns += [
    path('api-auth/', include('rest_framework.urls')),
]

if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT)