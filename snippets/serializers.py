from rest_framework import serializers

from snippets.models import Snippet
from users.serializers import UserSerializer


class SnippetSerializer(serializers.ModelSerializer):
    owner = UserSerializer(read_only=True)

    class Meta:
        model = Snippet
        fields = (
            'id',
            'title',
            'code',
            'linenos',
            'language',
            'style',
            'owner',
        )
