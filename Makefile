run:
	make down;\
	docker-compose run --service-ports nginx;\

shell:
	docker-compose run rest_app python manage.py shell;\

makemigrations:
	docker-compose run rest_app python manage.py makemigrations;\

migrate:
	docker-compose run rest_app python manage.py migrate;\

createsuperuser:
	docker-compose run rest_app python manage.py createsuperuser;\

rund:
	make down;\
	docker-compose run -d --service-ports rest_school;\

down:
	docker-compose down;\
