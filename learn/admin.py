from django.contrib import admin

from learn.models import Message, Theory, Lesson, Topic, Block, Course


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'text',)
    search_fields = ('text',)
    empty_value_display = '-пусто-'


@admin.register(Theory)
class TheoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'text',)
    search_fields = ('text',)
    empty_value_display = '-пусто-'


@admin.register(Lesson)
class LessonAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'type',)
    search_fields = ('title',)
    list_filter = ('type',)
    empty_value_display = '-пусто-'


@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'description', 'image',)
    search_fields = ('title', 'description',)
    empty_value_display = '-пусто-'


@admin.register(Block)
class BlockAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'description', 'image',)
    search_fields = ('title', 'description',)
    empty_value_display = '-пусто-'


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'description', 'image',)
    search_fields = ('title', 'description',)
    empty_value_display = '-пусто-'
