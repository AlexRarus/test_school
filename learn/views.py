from rest_framework import filters
from rest_framework.viewsets import ModelViewSet
import django_filters.rest_framework

from learn.permissions import StaffOrReadOnlyPermission
from learn.models import Message, Theory, Lesson, Topic, Block, Course
from learn.serializers import (
    MessageSerializer,
    TheorySerializer,
    LessonSerializer,
    TopicSerializer,
    BlockSerializer,
    CourseSerializer
)


class MessageViewSet(ModelViewSet):
    queryset = Message.objects.all().select_related(
        'author', 'lesson',).order_by('id')
    serializer_class = MessageSerializer
    permission_classes = (StaffOrReadOnlyPermission,)
    filter_backends = (
        django_filters.rest_framework.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    # filter_fields = ('lesson_id',)
    search_fields = ('text',)
    ordering_fields = ('id',)


class TheoryViewSet(ModelViewSet):
    queryset = Theory.objects.all().select_related('lesson').order_by('id')
    serializer_class = TheorySerializer
    permission_classes = (StaffOrReadOnlyPermission,)
    filter_backends = (
        django_filters.rest_framework.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    # filter_fields = ('lesson_id',)
    search_fields = ('text',)
    ordering_fields = ('id',)


class LessonViewSet(ModelViewSet):
    queryset = Lesson.objects.all().select_related('topic').prefetch_related(
        'messages',
        'theory'
    ).order_by('id')
    serializer_class = LessonSerializer
    permission_classes = (StaffOrReadOnlyPermission,)
    filter_backends = (
        django_filters.rest_framework.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    filter_fields = ('type', )
    search_fields = ('title', 'type', )
    ordering_fields = ('id',)


class TopicViewSet(ModelViewSet):
    queryset = Topic.objects.all().select_related('block').prefetch_related(
        'lessons',
    ).order_by('id')
    serializer_class = TopicSerializer
    permission_classes = (StaffOrReadOnlyPermission,)
    filter_backends = (
        django_filters.rest_framework.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    # filter_fields = ('block_id', )
    search_fields = ('title', 'description', )
    ordering_fields = ('id',)


class BlockViewSet(ModelViewSet):
    queryset = Block.objects.all().select_related('course').prefetch_related(
        'topics',
    ).order_by('id')
    serializer_class = BlockSerializer
    permission_classes = (StaffOrReadOnlyPermission,)
    filter_backends = (
        django_filters.rest_framework.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    # filter_fields = ('course_Iid', )
    search_fields = ('title', 'description', )
    ordering_fields = ('id',)


class CourseViewSet(ModelViewSet):
    queryset = Course.objects.all().select_related('owner').prefetch_related(
        'blocks',
    ).order_by('id')
    serializer_class = CourseSerializer
    permission_classes = (StaffOrReadOnlyPermission,)
    filter_backends = (
        django_filters.rest_framework.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    filter_fields = ('title', )
    search_fields = ('title', 'description', )
    ordering_fields = ('id',)

    def create(self, request, *args, **kwargs):
        request.data['owner'] = request.user
        return super().create(request, *args, **kwargs)
