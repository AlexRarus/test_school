from django.db import models

from users.models import User


class Course(models.Model):
    owner = models.ForeignKey(
        User,
        verbose_name='Создатель курса',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='courses',
    )
    title = models.CharField(
        'Название курса',
        max_length=255,
        null=False,
    )
    description = models.TextField('Описание курса')
    image = models.ImageField(
        verbose_name='Изображение курса',
        upload_to='api/images/',
        blank=True,
        null=True
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Курс'
        verbose_name_plural = 'Курсы'


class Block(models.Model):
    title = models.CharField(
        'Название блока',
        max_length=255,
        null=False,
    )
    description = models.TextField('Описание блока')
    image = models.ImageField(
        verbose_name='Изображение блока',
        upload_to='api/images/',
        blank=True,
        null=True
    )
    course = models.ForeignKey(
        Course,
        verbose_name='Курс',
        on_delete=models.SET_NULL,
        null=True,
        related_name='blocks',
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Блок курса'
        verbose_name_plural = 'Блоки курсов'


class Topic(models.Model):
    title = models.CharField(
        'Название темы',
        max_length=255,
        null=False,
    )
    description = models.TextField('Описание темы')
    image = models.ImageField(
        verbose_name='Изображение темы',
        upload_to='api/images/',
        blank=True,
        null=True
    )
    block = models.ForeignKey(
        Block,
        verbose_name='Блок',
        on_delete=models.SET_NULL,
        null=True,
        related_name='topics',
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Тема блока'
        verbose_name_plural = 'Темы блоков'


class Lesson(models.Model):
    TYPE_CHOICES = (
        (1, 'theory'),
        (2, 'dialog'),
    )
    title = models.CharField(
        'Заголовок',
        max_length=255,
        null=False,
    )
    type = models.PositiveIntegerField(choices=TYPE_CHOICES)
    topic = models.ForeignKey(
        Topic,
        verbose_name='Тема',
        on_delete=models.SET_NULL,
        null=True,
        related_name='lessons',
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Занятие'
        verbose_name_plural = 'Занятия'


class Theory(models.Model):
    text = models.TextField('Текст сообщения', null=False)
    lesson = models.ForeignKey(
        Lesson,
        verbose_name='Занятие',
        on_delete=models.SET_NULL,
        null=True,
        related_name='theory',
    )

    def __str__(self):
        return self.text[:15]

    class Meta:
        verbose_name = 'Теория'
        verbose_name_plural = 'Теории'


class Message(models.Model):
    author = models.ForeignKey(
        User,
        verbose_name='Автор сообщения',
        on_delete=models.SET_NULL,
        null=True,
        related_name='messages',
    )
    text = models.TextField('Текст сообщения', null=False)
    lesson = models.ForeignKey(
        Lesson,
        verbose_name='Занятие',
        on_delete=models.SET_NULL,
        null=True,
        related_name='messages',
    )

    def __str__(self):
        return self.text[:15]

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
