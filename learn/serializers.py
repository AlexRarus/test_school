from rest_framework import serializers
from django.contrib.auth import get_user_model

from learn.models import Course, Block, Topic, Lesson, Theory, Message


User = get_user_model()


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name',)


class MessageSerializer(serializers.ModelSerializer):
    author = AuthorSerializer(read_only=True)
    lesson_id = serializers.CharField(source='lesson.id', read_only=True)

    class Meta:
        model = Message
        fields = ('id', 'text', 'author', 'lesson_id',)


class TheorySerializer(serializers.ModelSerializer):
    lesson_id = serializers.CharField(source='lesson.id', read_only=True)

    class Meta:
        model = Theory
        fields = ('id', 'text', 'lesson_id',)


class LessonSerializer(serializers.ModelSerializer):
    theory = TheorySerializer(many=True, read_only=True)
    messages = MessageSerializer(many=True, read_only=True)
    topic_id = serializers.CharField(source='topic.id', read_only=True)

    class Meta:
        model = Lesson
        fields = ('id', 'title', 'type', 'theory', 'messages', 'topic_id',)


class TopicSerializer(serializers.ModelSerializer):
    lessons = LessonSerializer(many=True, read_only=True)
    block_id = serializers.CharField(source='block.id', read_only=True)

    class Meta:
        model = Topic
        fields = (
            'id', 'title', 'description', 'image', 'lessons', 'block_id',)


class BlockSerializer(serializers.ModelSerializer):
    topics = TopicSerializer(many=True, read_only=True)
    course_id = serializers.CharField(source='course.id', read_only=True)

    class Meta:
        model = Block
        fields = (
            'id', 'title', 'description', 'image', 'topics', 'course_id',)


class CourseSerializer(serializers.ModelSerializer):
    blocks = BlockSerializer(many=True, read_only=True)
    owner = AuthorSerializer(read_only=True)

    class Meta:
        model = Course
        fields = ('id', 'title', 'description', 'image', 'blocks', 'owner', )
